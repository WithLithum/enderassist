# EnderAssist

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/WithLithum/enderassist?branch=trunk&logo=circleci&style=flat-square)
![GitLab issues](https://img.shields.io/gitlab/issues/open/WithLithum/enderassist?style=flat-square)
![GitLab merge requests](https://img.shields.io/gitlab/merge-requests/open/WithLithum/enderassist?style=flat-square)
![GitLab tag (latest by date)](https://img.shields.io/gitlab/v/tag/WithLithum/enderassist?style=flat-square)

EnderAssist 是适用于 WithLithum 的服务器的客制化插件。